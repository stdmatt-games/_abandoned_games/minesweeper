
//----------------------------------------------------------------------------//
// Flag Particle                                                              //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const FLAG_PARTICLE_ANIM_ADD_TIME    = 0.1;
const FLAG_PARTICLE_ANIM_REMOVE_TIME = 2.5;

const FLAG_PARTICLE_ANIM_FORCE_MIN_X = -20;
const FLAG_PARTICLE_ANIM_FORCE_MAX_X = +20;

const FLAG_PARTICLE_ANIM_FORCE_MIN_Y = (-120 * 0.9);
const FLAG_PARTICLE_ANIM_FORCE_MAX_Y = (-90  * 0.9);

const FLAG_PARTICLE_ANIM_ANGLE_MIN = -MATH_PI;
const FLAG_PARTICLE_ANIM_ANGLE_MAX = +MATH_PI;

const GRAVITY = 35;
const FLAG_PARTICLE_FORCE_DECAY = 0.9;

//------------------------------------------------------------------------------
class FlagParticle
    extends PIXI.Sprite
{
    //--------------------------------------------------------------------------
    constructor()
    {
        super(Texture_Get("flag_icon"));

        //
        // iVars
        // Properties.
        this.rotation    = 0;
        this.targetScale = 1;

        this.isAdding   = false;
        this.isRemoving = false;
        this.isDone     = false;

        this.animationTimer = new Base_Timer(0);

        this.removeAnimationForce    = this._CalcRandomRemoveForce   ();
        this.removeAnimationAngle    = this._CalcRemoveAnimationAngle();
        this.removeAnimationVelocity = Vector_Create(0, 0);
    } // ctor

    //--------------------------------------------------------------------------
    SetTargetScale(s)
    {
        this.targetScale = s;
    }

    //--------------------------------------------------------------------------
    StartAddAnimation()
    {
        this.isAdding   = true;
        this.isRemoving = false;

        this.animationTimer.duration = FLAG_PARTICLE_ANIM_ADD_TIME;
        this.animationTimer.Start();

        this.scale.set(0);
    } // StartAddAnimation

    //--------------------------------------------------------------------------
    StartRemoveAnimation()
    {
        this.isAdding   = false;
        this.isRemoving = true;

        this.animationTimer.duration = FLAG_PARTICLE_ANIM_REMOVE_TIME;
        this.animationTimer.Start();
    } // StartRemoveAnimation

    //--------------------------------------------------------------------------
    Update(dt)
    {
        if(this.isDone && (!this.isAdding || !this.isRemoving)) {
            return;
        }

        this.animationTimer.Update(dt);

        if(this.isAdding) {
            this.scale.set(this.animationTimer.ratio * this.targetScale);
        } else if(this.isRemoving) {
            let scale = (1 - this.animationTimer.ratio) * this.targetScale
            this.scale.set(scale);

            this.removeAnimationForce.x *= FLAG_PARTICLE_FORCE_DECAY; // decay...
            this.removeAnimationForce.y *= FLAG_PARTICLE_FORCE_DECAY; // decay...

            let accX =  this.removeAnimationForce.x;
            let accY  = this.removeAnimationForce.y + GRAVITY;

            this.removeAnimationVelocity.x += accX;
            this.removeAnimationVelocity.y += accY;

            this.position.x += this.removeAnimationVelocity.x * dt;
            this.position.y += this.removeAnimationVelocity.y * dt;
            this.rotation   += this.removeAnimationAngle      * dt;

            if(this.animationTimer.isDone) {
                this.isRemoving = false;
                this.isDone     = true;
            }
        }
    } // Update


    //--------------------------------------------------------------------------
    _CalcRandomRemoveForce()
    {
        let x = Random_Int(
            FLAG_PARTICLE_ANIM_FORCE_MIN_X,
            FLAG_PARTICLE_ANIM_FORCE_MAX_X
        );
        let y = Random_Int(
            FLAG_PARTICLE_ANIM_FORCE_MIN_Y,
            FLAG_PARTICLE_ANIM_FORCE_MAX_Y
        );

        return Vector_Create(x, y);
    } // _CalcRandomRemoveForce

     //--------------------------------------------------------------------------
     _CalcRemoveAnimationAngle()
     {
         return Math_Map(
             this.removeAnimationForce.x,
             FLAG_PARTICLE_ANIM_FORCE_MIN_X,
             FLAG_PARTICLE_ANIM_FORCE_MAX_X,
             FLAG_PARTICLE_ANIM_ANGLE_MIN,
             FLAG_PARTICLE_ANIM_ANGLE_MAX
         );
     } // _CalcRemoveAnimationAngle
}; // class FlagParticle
