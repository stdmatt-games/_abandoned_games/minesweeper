//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Board.js                                                      //
//  Project   : Minesweeper                                                   //
//  Date      : Aug 03, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Helper Functions                                                           //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function GetSurroundCoords(v)
{
    let coords = [];
    for(let i = -1; i <= +1; ++i) {
        let yy = v.y + i;
        for(let j = -1; j <= +1; ++j) {
            let xx = v.x + j;

            if(yy == v.y && xx == v.x) { // Don't want the same coord.
                continue;
            }
            coords.push(Vector_Create(xx, yy));
        }
    }

    return coords;
}

//------------------------------------------------------------------------------
function GetAdjacentCoords(v)
{
    let coords = [];
    coords.push(Vector_Create(v.x +0, v.y +1)); // Bottom
    coords.push(Vector_Create(v.x +1, v.y +0)); // Right
    coords.push(Vector_Create(v.x -1, v.y +0)); // Top
    coords.push(Vector_Create(v.x +0, v.y -1)); // Left
    return coords;
}

//----------------------------------------------------------------------------//
// Board                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const GAME_STATE_CONTINUE                = 0;
const GAME_STATE_VICTORY                 = 1;
const GAME_STATE_DEFEAT_OPENING_BOMBS    = 2;
const GAME_STATE_DEFEAT_OPENING_FLAGS    = 3;
const GAME_STATE_DEFEAT_OPENING_FINISHED = 4;

const BOARD_SHAKE_TIME  = 1;
const BOARD_SHAKE_FREQ  = 15;
const BOARD_SHAKE_SCALE = 5;

const BOARD_BOMB_OPEN_TIMER_DURATION = 0.1;

//------------------------------------------------------------------------------
class Board
    extends PIXI.Container
{
    //--------------------------------------------------------------------------
    constructor(desiredSize, blockCount, minesCount)
    {
        super();

        this.gameState = GAME_STATE_CONTINUE;

        this.desiredSize = desiredSize;
        this.blocksCount = blockCount;
        this.minesCount  = minesCount;
        this.flagsCount  = 0;

        this.blocksGrid = null; // The grid field.
        this.bombBlocks = null; // Array with the blocks that are bombs.
        this.blockSize  = null; // The size of the block for this board.

        this.flagParticles  = []; // All the flags  that need to be updated
        this.coverParticles = []; // All the covers that need to be updated.
        this.bombParticles  = []; // All the bombs  that need to be updated.

        this.blocksGridTexture = new PIXI.Graphics();
        this.shakeX = new Base_Shake(BOARD_SHAKE_TIME, BOARD_SHAKE_FREQ);
        this.shakeY = new Base_Shake(BOARD_SHAKE_TIME, BOARD_SHAKE_FREQ);

        this.bombOpenTimer = new Base_Timer(BOARD_BOMB_OPEN_TIMER_DURATION);

        // Init
        this._CreateBlocks           ();
        this._PlaceBombs             ();
        this._CreateBlocksGridTexture();

        this.addChildAt(this.blocksGridTexture, 0);
    } // ctor

    //--------------------------------------------------------------------------
    OnMouseClick(pos)
    {
        if(this.gameState != GAME_STATE_CONTINUE) {
            return;
        }

        const coord = Vector_Create(
            Math_Int(pos.x / this.blockSize.x),
            Math_Int(pos.y / this.blockSize.y)
        );

        if(!this._IsValidCoord(coord)) {
            return;
        }

        let block = this.blocksGrid[coord.y][coord.x];
        if(block.isOpened || block.isFlagged) {
            return;
        }

        // @XXX(stdmatt): Hacky... to make the Z index correct...
        this.removeChild(block.cover);
        this.addChild   (block.cover);

        this._FloodFill(block);
        if(block.isBomb) {
            this.gameState = GAME_STATE_DEFEAT_OPENING_BOMBS;
            this.bombOpenTimer.Start();
        }
    } // OnMouseClick

    //--------------------------------------------------------------------------
    OnMouseRightClick(pos)
    {
        if(this.gameState != GAME_STATE_CONTINUE) {
            return;
        }

        const coord = Vector_Create(
            Math_Int(pos.x / this.blockSize.x),
            Math_Int(pos.y / this.blockSize.y)
        );

        if(!this._IsValidCoord(coord)) {
            return;
        }

        let block = this.blocksGrid[coord.y][coord.x];
        block.ToggleFlag();
    } // OnMouseRightClick


    //--------------------------------------------------------------------------
    AddUpdatingCovers(cover)
    {
        this.coverParticles.push(cover);
    } // AddUpdatingCovers

    //--------------------------------------------------------------------------
    AddUpdatingFlags(flag)
    {
        this.flagParticles.push(flag);
    } // AddUpdatingFlags

    //--------------------------------------------------------------------------
    AddUpdatingBombs(bomb)
    {
        this.bombParticles.push(bomb);
    } // AddUpdatingBombs


    //--------------------------------------------------------------------------
    Update(dt)
    {
        // Shake
        this.shakeX.Update(dt);
        this.shakeY.Update(dt);

        // Covers
        this._UpdateRemoveLoop(this.coverParticles);
        this._UpdateRemoveLoop(this.flagParticles);
        this._UpdateRemoveLoop(this.bombParticles);


        if(this.gameState == GAME_STATE_DEFEAT_OPENING_BOMBS) {
            this.bombOpenTimer.Update(dt);
            if(this.bombOpenTimer.isDone) {
                if(this.bombBlocks.length > 0) {
                    let bomb_block = this.bombBlocks[0];

                    this._FloodFill(bomb_block);
                    this.bombOpenTimer.Start();

                    Array_RemoveAt(this.bombBlocks, 0);
                } else {
                    this.gameState = GAME_STATE_DEFEAT_OPENING_FLAGS;
                }
            }
        } else if(this.gameState == GAME_STATE_DEFEAT_OPENING_FLAGS) {
            for(let i = 0; i < this.flagParticles.length; ++i) {
                const flag = this.flagParticles[i];
                const flag_index_x = Math_Int(flag.x / this.blockSize.x);
                const flag_index_y = Math_Int(flag.y / this.blockSize.y);

                const block = this.blocksGrid[flag_index_y][flag_index_x];
                block.AddWrongFlagMark();
            }
            this.gameState = GAME_STATE_DEFEAT_OPENING_FINISHED;
        }
    } // Update

    //--------------------------------------------------------------------------
    _UpdateRemoveLoop(container)
    {
        for(let i = container.length-1; i >= 0; --i) {
            let obj = container[i];
            obj.Update(dt);
            if(obj.isDone) {
                obj.parent.removeChild(obj);
                Array_RemoveAt(container, i);
            }
        }
    } // _UpdateRemoveLoop

    //--------------------------------------------------------------------------
    _FloodFill(block)
    {
        if(block.isOpened || block.isFlagged) {
            return;
        }

        block.Open();
        if(!this.shakeX.started) {
            this.shakeX.Start();
            this.shakeY.Start();
        }

        // Open Edges
        {
            let adjacent_coords = GetAdjacentCoords(block.coordInBoard);
            for(let i = 0; i < adjacent_coords.length; ++i) {
                let coord = adjacent_coords[i];
                if(!this._IsValidCoord(coord)) {
                    continue;
                }

                let adjacent_block = this.blocksGrid[coord.y][coord.x];
                if(adjacent_block.cover != null) {
                    let v = Vector_Sub(block.coordInBoard, adjacent_block.coordInBoard);
                    adjacent_block.cover.OpenEdge(v);
                }
            }
        }

        if(block.value != BLOCK_VALUE_EMPTY) {
            return;
        }

        let surround_coords = GetSurroundCoords(block.coordInBoard);
        for(let i = 0; i < surround_coords.length; ++i) {
            let coord = surround_coords[i];
            if(!this._IsValidCoord(coord)) {
                continue;
            }

            let surround_block = this.blocksGrid[coord.y][coord.x];
            this._FloodFill(surround_block);
        }
    } // _FloodFill

    //--------------------------------------------------------------------------
    _CreateBlocks()
    {
        this.blocksGrid = Array_Create2D(this.blocksCount.y, this.blocksCount.x);

        // @TODO(stdmatt): better way to calculate the size of the block...
        this.blockSize = Vector_Create(
            this.desiredSize.x / this.blocksCount.x,
            this.desiredSize.y / this.blocksCount.y
        );
        for(let i = 0; i < this.blocksCount.y; ++i) {
            for(let j = 0; j < this.blocksCount.x; ++j) {
                this.blocksGrid[i][j] = new Block(j, i, this);
            }
        }
    } // _CreateBlocks

    //--------------------------------------------------------------------------
    _PlaceBombs()
    {
        this.bombBlocks = [];

        let mines_to_place = this.minesCount;  // Need to create a copy to decrement.
        while(mines_to_place != 0) {
            let x = Random_Int(0, this.blocksCount.x);
            let y = Random_Int(0, this.blocksCount.y);

            let block = this.blocksGrid[y][x];
            if(block.value == BLOCK_VALUE_BOMB) {
                continue;
            }

            block.SetAsBomb();
            this.bombBlocks.push(block);
            --mines_to_place;

            let surround_coords = GetSurroundCoords(block.coordInBoard);
            for(let i = 0; i < surround_coords.length; ++i) {
                let coord = surround_coords[i];
                if(!this._IsValidCoord(coord)) {
                    continue;
                }

                let surround_block = this.blocksGrid[coord.y][coord.x];
                if(surround_block.value == BLOCK_VALUE_BOMB) {
                    continue;
                }
                ++surround_block.value;
            }
        }
    } // _PlaceBombs

    //--------------------------------------------------------------------------
    _CreateBlocksGridTexture()
    {
        for(let i = 0; i < this.blocksCount.y; ++i) {
            for(let j = 0; j < this.blocksCount.x; ++j) {
                this._RenderBlockOnGridTexture(this.blocksGrid[i][j]);
            }
        }
    }

    //--------------------------------------------------------------------------
    _RenderBlockOnGridTexture(block)
    {
        const block_x = (block.coordInBoard.x * this.blockSize.x);
        const block_y = (block.coordInBoard.y * this.blockSize.y);
        const block_w = this.blockSize.x;
        const block_h = this.blockSize.y;

        if(block.isBomb) {
            // Background
            let color = block.bombColor;
            this.blocksGridTexture.beginFill(color.num(), 1);
                this.blocksGridTexture.drawRect(block_x, block_y, block_w, block_h);
            this.blocksGridTexture.endFill();

            // Bomb
            color = color.darken(2);
            this.blocksGridTexture.beginFill(color.num(), 1);
                this.blocksGridTexture.drawCircle(
                    block_x + block_w / 2,
                    block_y + block_h / 2,
                    block_w / 6
                );
            this.blocksGridTexture.endFill();
        } else {
            // Background
            let color = gPalette.GetBlockOpenedColor(block.isLight);
            this.blocksGridTexture.beginFill(color.num(), 1);
                this.blocksGridTexture.drawRect(block_x, block_y, block_w, block_h);
            this.blocksGridTexture.endFill();
            // Number
            if(block.value != BLOCK_VALUE_EMPTY) {
                color = gPalette.getBlockValueColor(block.value);

                const font_size = block_h * 0.7;
                const text = new PIXI.Text(
                    block.value,
                    {
                        fontFamily : 'Arial',
                        fontSize   : font_size,
                        fill       : color.num(),
                    }
                );
                text.anchor.set(0.5);
                text.x = block_x + block_w / 2;
                text.y = block_y + block_h / 2;
                this.blocksGridTexture.addChild(text);
            }
        }
    } // _RenderBlockOnGridTexture

    //--------------------------------------------------------------------------
    _IsValidCoord(coord)
    {
        return coord.x >= 0 && coord.x < this.blocksCount.x
            && coord.y >= 0 && coord.y < this.blocksCount.y;
    } // _IsValidCoord

    //--------------------------------------------------------------------------
    _ascii()
    {
        let s = "";
        for(let i = 0; i < this.blocksCount.y; ++i) {
            for(let j = 0; j < this.blocksCount.x; ++j) {
                let block = this.blocksGrid[i][j];
                if(block.value == BLOCK_VALUE_EMPTY) {
                    s += ".";
                } else if(block.value == BLOCK_VALUE_BOMB) {
                    s += "*";
                } else {
                    s += block.value;
                }
            }
            s += "\n";
        }
        console.log(s);
    } // _ascii
}; // Board
