//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Minesweeper.js                                                //
//  Project   : Minesweeper                                                   //
//  Date      : Aug 02, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//   Just a simple minesweeper game...                                        //
//---------------------------------------------------------------------------~//

//----------------------------------------------------------------------------//
// Constants                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const GAME_DESIGN_WIDTH  = 700;
const GAME_DESIGN_HEIGHT = 700;
const GAME_HUD_HEIGHT    = 40;

//----------------------------------------------------------------------------//
// Globals                                                                    //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
let gPalette = null;
let gBoard   = null;
let gGameHud = null;

//----------------------------------------------------------------------------//
// Functions                                                                  //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function NewGame(mode)
{
    let board_rows   = 0;
    let board_cols   = 0;
    let mines_count  = 0;
    let board_width  = 0;
    let board_height = 0;

    if(mode == 1) { // Easy
        board_rows   = 8;
        board_cols   = 10;
        mines_count  = 10;
        board_width  = 440;
        board_height = 352;
    } else if(mode == 2) { // Medium
        board_rows   = 14;
        board_cols   = 18;
        mines_count  = 40;
        board_width  = 540;
        board_height = 420;
    } else if(mode == 3) { // Hard
        board_rows   = 20;
        board_cols   = 24;
        mines_count  = 99;
        board_width  = 600;
        board_height = 500;
    }

    g_App.renderer.resize(board_width, board_height + GAME_HUD_HEIGHT);

    if(gGameHud) {
        gGameHud.parent.removeChild(gGameHud);
    }
    if(gBoard) {
        gBoard.parent.removeChild(gBoard);
    }

    gGameHud = new StatusHud(
        Vector_Create(board_width, GAME_HUD_HEIGHT)
    );
    gBoard = new Board(
        Vector_Create(board_width, board_height),
        Vector_Create(board_cols,  board_rows),
        mines_count
    );
    gBoard.y = (gGameHud.y + gGameHud.height);

    g_App.stage.addChild(gGameHud);
    g_App.stage.addChild(gBoard  );

    gBoard._ascii();
}


//----------------------------------------------------------------------------//
// Setup / Draw                                                               //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function PreInit()
{
    Application_Create(
        GAME_DESIGN_WIDTH,
        GAME_DESIGN_HEIGHT,
        GameLoop
    );
}


//------------------------------------------------------------------------------
async function Preload()
{
    Texture_SetBasePath("res/");
    PIXI_LOADER.add([
        "res/clock_icon.png",
        "res/flag_icon.png",
        "res/wrong_icon.png",
    ]).load(Setup);
}

//------------------------------------------------------------------------------
function Setup()
{
    Random_Seed(1);

    Install_MouseHandlers   ();
    Install_KeyboardHandlers();

    g_App.stage.interactive = true;
    g_App.stage.buttonMode  = true;

    gPalette = new Palette();

    NewGame(1);
}


//------------------------------------------------------------------------------
function GameLoop(delta)
{
    // gBoard    .Update(delta);
    // gStatusHud.Update(delta);
    if(gBoard != null) {
        gBoard.Update(delta);
    }

}

//----------------------------------------------------------------------------//
// Input Handlers                                                             //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
function MouseMove(e)
{
}

//------------------------------------------------------------------------------
function MouseClick(e)
{
    const pos = e.getLocalPosition(gBoard);
    gBoard.OnMouseClick(pos);
}

//------------------------------------------------------------------------------
function MouseRightClick(e)
{
    const pos = e.getLocalPosition(gBoard);
    gBoard.OnMouseRightClick(pos);
}

//------------------------------------------------------------------------------
function KeyboardDown(e)
{
}

//------------------------------------------------------------------------------
function KeyboardUp(e)
{
}

//----------------------------------------------------------------------------//
// Entry Point                                                                //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
PreInit();
Preload();
