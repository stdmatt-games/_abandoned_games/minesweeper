//~---------------------------------------------------------------------------//
//                        _      _                 _   _                      //
//                    ___| |_ __| |_ __ ___   __ _| |_| |_                    //
//                   / __| __/ _` | '_ ` _ \ / _` | __| __|                   //
//                   \__ \ || (_| | | | | | | (_| | |_| |_                    //
//                   |___/\__\__,_|_| |_| |_|\__,_|\__|\__|                   //
//                                                                            //
//  File      : Board.js                                                      //
//  Project   : Minesweeper                                                   //
//  Date      : Aug 03, 2019                                                  //
//  License   : GPLv3                                                         //
//  Author    : stdmatt <stdmatt@pixelwizards.io>                             //
//  Copyright : stdmatt - 2019                                                //
//                                                                            //
//  Description :                                                             //
//                                                                            //
//---------------------------------------------------------------------------~//
//----------------------------------------------------------------------------//
// Block                                                                      //
//----------------------------------------------------------------------------//
//------------------------------------------------------------------------------
const BLOCK_VALUE_EMPTY           =  0;
const BLOCK_VALUE_BOMB            = -1
const BLOCK_BOMBS_PARTICLES_COUNT = 10;

//------------------------------------------------------------------------------
class Block
{
    //--------------------------------------------------------------------------
    constructor(indexX, indexY, boardRef)
    {
        //
        // iVars
        // Refs
        this.boardRef = boardRef;
        // Properties.
        this.coordInBoard = Vector_Create(indexX, indexY);
        this.value        = BLOCK_VALUE_EMPTY;

        this.isOpened   = false;
        this.isFlagged  = false;
        this.isBomb     = false;
        this.isLight    = (indexY % 2 == 0 && indexX % 2 == 0)
                       || (indexY % 2 != 0 && indexX % 2 != 0);

        this.cover = null;
        this.flag  = null;

        //
        // Initialize.
        this._CreateCoverParticleForBlock();
    } // ctor

    //--------------------------------------------------------------------------
    SetAsBomb()
    {
        this.value     = BLOCK_VALUE_BOMB;
        this.isBomb    = true;
        this.bombColor = gPalette.GetRandomBombColor();
    } // SetAsBomb

    //--------------------------------------------------------------------------
    Open()
    {
        if(this.isFlagged || this.isOpened) {
            return;
        }

        this.isOpened  = true;
        this.isFlagged = false;

        if(this.cover != null) {
            this.cover.StartOpenAnimation();
            this.boardRef.AddUpdatingCovers(this.cover);
            this.cover = null;
        }

        if(this.isBomb && !this.isFlagged) {
            this._CreateBombParticleForBlock();
        }
    } // Open

    //--------------------------------------------------------------------------
    ToggleFlag()
    {
        if(this.isOpened) {
            return;
        }

        if(this.flag != null) {
            this.flag.StartRemoveAnimation();
            this.flag = null;
        }

        this.isFlagged = !this.isFlagged;
        if(this.isFlagged) {
            this._CreateFlagParticleForBlock();
            this.flag.StartAddAnimation();
        }
    } // ToggleFlag

    //--------------------------------------------------------------------------
    AddWrongFlagMark()
    {
        if(this.isFlagged && this.isBomb) {
            return;
        }

        this.ToggleFlag();
        let sprite = Sprite_Create("wrong_icon");

        const block_w = this.boardRef.blockSize.x;
        const block_h = this.boardRef.blockSize.y;
        const pos_x   = (this.coordInBoard.x * block_w);
        const pos_y   = (this.coordInBoard.y * block_h);
        const scale   = (block_h / sprite.height) * 0.7;

        sprite.anchor.set(0.5, 0.5);
        sprite.scale.set(scale, scale);
        sprite.x      = pos_x + (block_w / 2);
        sprite.y      = pos_y + (block_h / 2);
        sprite.tint = 0xFF0000;

        this.boardRef.addChild(sprite);
    }

    //--------------------------------------------------------------------------
    _CreateCoverParticleForBlock()
    {
        const block_w = this.boardRef.blockSize.x;
        const block_h = this.boardRef.blockSize.y;
        const pos_x   = (this.coordInBoard.x * block_w);
        const pos_y   = (this.coordInBoard.y * block_h);

        let cover = new CoverParticle(
            block_w, block_h,
            gPalette.GetBlockClosedColor(this.isLight)
        );
        this.cover = cover;

        cover.anchor.set(0.5, 0.5);
        cover.x = pos_x + (block_w / 2);
        cover.y = pos_y + (block_h / 2);

        this.boardRef.addChild(cover);
    } // _CreateCoverParticleForBlock


    //--------------------------------------------------------------------------
    _CreateFlagParticleForBlock()
    {
        let flag = new FlagParticle();
        this.flag = flag;

        const block_w = this.boardRef.blockSize.x;
        const block_h = this.boardRef.blockSize.y;
        const pos_x   = (this.coordInBoard.x * block_w);
        const pos_y   = (this.coordInBoard.y * block_h);
        const scale   = (block_h / flag.height);

        flag.SetTargetScale(scale * 0.7);
        flag.anchor.set(0.5, 0.5);
        flag.x    = pos_x + (block_w / 2);
        flag.y    = pos_y + (block_h / 2);
        flag.tint = 0xFF0000;

        this.boardRef.addChild(flag);
        this.boardRef.AddUpdatingFlags(flag);
    } // _CreateFlagParticleForBlock

    //--------------------------------------------------------------------------
    _CreateBombParticleForBlock()
    {
        const block_w = this.boardRef.blockSize.x;
        const block_h = this.boardRef.blockSize.y;
        const pos_x   = (this.coordInBoard.x * block_w);
        const pos_y   = (this.coordInBoard.y * block_h);

        for(let i = 0; i < BLOCK_BOMBS_PARTICLES_COUNT; ++i) {
            let bomb = new BombParticle(this.bombColor);

            bomb.x = pos_x + Random_Int(-block_w, +block_w)
            bomb.y = pos_y + Random_Int(-block_h, +block_h);
            bomb.StartOpenAnimation();

            this.boardRef.addChild(bomb);
            this.boardRef.AddUpdatingBombs(bomb);
        }
    } // _CreateBombParticleForBlock
}; // class Block
